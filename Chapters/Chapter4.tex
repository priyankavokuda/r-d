\chapter{Implementation details: Object Classification }
\lhead{\emph{Implementation details: Object Classification }} 

In the following sections of this chapter, we reiterate the details from chapter 3 of our previous work \cite{VokudaRnD16} for the sake of continuity.

\section{Overview}

%In Multilayer perceptron (MLP) as the "depth" of perceptron increases, the number of parameters add up which quickly become unmanagable and also leads to overfitting.  For example, if the images are of size 227$ \times $227$ \times $3(227 wide, 227 high, 3 colour channels), so only a single fully connected neuron in a first hidden layer of a regular neural network would have 227$ \times $227$ \times $3 = 154,587 weights. Thus MLP does not scale well to higher resolution images. 

Convolutional neural networks basically emulate the behavior of a visual cortex \cite{vcortex}.
The layers of CNN have three dimensions - width, height and depth (activation volume). Although the parameters in CNNs are not as high as Multilayer perceptron (MLP), but there are still a large number of parameters when in a large stack of layers. CNNs make use of the property that if one filter is useful to compute some feature, then the same filter should also be useful to compute similar feature at a different position. In other words, we to constrain the neurons in each depth slice we use the same weights and bias. This property, called parameter sharing, also makes CNNs translation invariant by detecting features regardless of their position in the image. CNNs also take into consideration that images are spatially locally correlated by having a local connectivity pattern between neurons of adjacent layers. 


These properties allow CNNs to achieve a better solution on vision problems. Considering these advantages, we use CNNs to solve our object classification problem. We use ImageNet \cite{imagenetcnn} trained network by Caffe called CaffeNet model \cite{caffeimagenet} for our training which is shown in Figure: \ref{fig:caffenet1} and \ref{fig:caffenet2}.

% \begin{sidewaysfigure}[h!]
%     \includegraphics[width=1.1\textwidth]{Images/caffenet_1.png}
%     \caption{Caffenet model for RGB-D Object Dataset \cite{lai} (part 1) }
%     \label{fig:caffenet1}
% \end{sidewaysfigure}

\begin{sidewaysfigure}
  \centering
    \includegraphics[width=1.0\textwidth,height=6cm]{Images/caffenet_1.png}
    \caption{Caffenet model for RGB-D Object Dataset \cite{lai} (part 1)}
  \label{fig:caffenet1}
    \includegraphics[width=1.0\textwidth,height=6cm]{Images/caffenet_2.png}
    \caption{Caffenet model for RGB-D Object Dataset \cite{lai} (part 2) }
  \label{fig:caffenet2}
\end{sidewaysfigure}


% \begin{figure}[h!]
%     \centering
%     \includegraphics[width=1.1\textwidth]{Images/caffenet_1.png}
%     \caption{Caffenet model for RGB-D Object Dataset \cite{lai} (part 1) }
%     \label{fig:caffenet1}
% \end{figure}
% \begin{figure}[h!]
%     \centering
%     \includegraphics[width=1.1\textwidth]{Images/caffenet_2.png}
%     \caption{Caffenet model for RGB-D Object Dataset \cite{lai} (part 2) }
%     \label{fig:caffenet2}
% \end{figure}

Our approach as shown in Figure \ref{fig:architecture} contains two streams, top-blue for RGB and bottom-green for depth data which are fused using concatenate layer. The input of the network are RGB and depth images of size 227 $ \times $ 227 $ \times $ 3. Each stream (both blue and green) is a Caffenet model which consists of five Convolutional layers (CONV), seven Rectified Linear Units (ReLU), three Pooling layers (POOL), two Local Response Normalization layers (LRN), three Fully Connected layers (FC) and two Dropout layers as shown in Figures \ref{fig:caffenet1} and \ref{fig:caffenet2}. Both streams converge in one fully connected layer and a Softmax classifier (in gray). This approach is inspired by the work proposed in \cite{multimodal}. A pre-processing step is applied to RGB and depth images to suit the network. 

As we discussed earlier, there are millions of parameters in CNNs. While training, we use gradient descent method to find a local optimum. This process is sensitive to initialization. Therefore we use pre-trained weights for Caffenet to initialize a classifier. This process is called fine-tuning. To train the fused network we use fine-tuning on each network until the fusion layer and then jointly train the parameters of the network. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=1.0\textwidth]{Images/architecture.png}
    \caption{Two stream CNN network for RGB-D object Detection. "Blue" stream for RGB images and "green" stream for depth images. Each stream is a Caffenet model but for simplicity ReLU, POOL, LRN, Dropout, Softmax layers are not shown. }
    \label{fig:architecture}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\section{Input pre-processing}

\subsection{Image scaling}

Caffenet expects RGB images of size 227 $ \times $ 227 $ \times $ 3. So we first perform rescaling of data to appropriate size. The naive approach was to directly rescaling the images to required dimensions. The original images in the dataset were smaller size than 227 $ \times $ 227 and images were always not square shaped. This resulted in information loss of shape due to rescale and the classification performance was not good. So we employed a rescaling approach described in \cite{multimodal}. We scale the longest side of the original image to 227 pixels, resulting in a 227 $ \times $ N or an N $ \times $ 227 sized image, where N is the length of the shorter side. We also scale the shorter side of the image proportionally as the change in longer side. We then tile the borders of the shorter side ?. The scaling is graphically depicted in Figure \ref{fig:scaling}.  The same scaling operation is applied to both RGB and depth images.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/scaling.png}
    \caption{Comparing lossy scaling with lossless scaling approach for RGB and depth images.}
    \label{fig:scaling}
\end{figure}


\subsection{Colourisation of depth}
Caffenet is trained on RGB images. So RGB images do not need further pre-processing before starting the training. But the network does not perform well on depth images because they have distances rather than intensity values. So to make depth images compatible with the network, pre-processing was performed. The depth value was normalized to lie between 0 and 255. The normalized depth value was just replicated to three channels to mimic RGB images. But the classification performance was still poor and we attribute it to the fact that Caffenet is trained on color images. So we ``colorize'' the depth images using approach explained in \cite{multimodal}. We color the normalized depth image by mapping each pixel distance to color values ranging from red (near) over green to blue (far). This distributes the depth information over all three RGB channels. The depth colorization is graphically depicted in Figure \ref{fig:depthpreprocess}. To the normalized three channeled depth images, we apply a transfer function to R, G and B channels to map each distance to a color value. For example, for the R channel, if the value is 0, then it is mapped to 255 and if the value is 128, it is mapped to 0 and the intermediate values are mapped accordingly. Similar transfer function is applied to each channel to obtain a colorized depth image. The application of transfer function is further explained in figure \ref{fig:tf}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/colorization.png}
    \caption{Depth ``colorizing'' process. The raw depth image is normalized to a value between 0-255 and then each pixel distance to color values ranging from red (near) over green to blue (far).}
    \label{fig:depthpreprocess}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=1.0\textwidth]{Images/tf.png}
    \caption{Depth ``colorizing'' process explained mathematically. The line in the red color depicts the transfer function of red channel, line in green depicts the transfer function of green channel and line in blue depicts the transfer function of the blue channel. }
    \label{fig:tf}
\end{figure}

\subsection{Mean Subtraction}

The RGB and Depth images were converted to LMDB (Lightning Memory-Mapped Database) database format for memory efficient reading. Mean subtraction was done to center the data. The Caffenet model also requires us to subtract the image mean from each image.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\section{Network Training}

\subsection{CNN architecture}

The main layers which build CNN architectures are: Convolutional Layer, Rectified Linear Unit, Pooling Layer and Fully-Connected Layer. The most common form of a CNN architecture consists of a few CONV-ReLU layers, followed by POOL layers and a repetition of these patterns. Some architectures have FC layers at the end. The last fully-connected layer stores the output, in this case, class scores. CaffeNet has other layers such as Input layer, Local Response Normalization layer, Dropout, Accuracy, SoftMax. The output volume and the parameters of each layer in Caffenet is given in Table \ref{table:caffenet}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Please add the following required packages to your document preamble:
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[h!]
\centering
\begin{tabular}{|
>{\columncolor[HTML]{EFEFEF}}l |l|c|c|c|c|c|}
\hline
\cellcolor[HTML]{EFEFEF}\textbf{Layer} & \cellcolor[HTML]{EFEFEF}\textbf{\begin{tabular}[c]{@{}l@{}}Output \\ volume\end{tabular}} & \cellcolor[HTML]{EFEFEF}\textbf{\begin{tabular}[c]{@{}l@{}}No. \\ of filters\end{tabular}} & \cellcolor[HTML]{EFEFEF}\textbf{\begin{tabular}[c]{@{}l@{}}Filter \\ size\end{tabular}} & \cellcolor[HTML]{EFEFEF}\textbf{Stride} & \cellcolor[HTML]{EFEFEF}\textbf{Padding} & \cellcolor[HTML]{EFEFEF}\textbf{Weights} \\ \hline
INPUT                                  & {[}227$ \times $227$ \times $3{]}                                                                           &                                                                                            &                                                                                        &                                        &                                         &                                         \\ \hline
CONV1                                  & {[}55$ \times $55$ \times $96{]}                                                                            & 96                                                                                         & 11$ \times $11                                                                                   & 4                                       & 0                                        & 11$ \times $11$ \times $3$ \times $96                               \\ \hline
POOL1                                  & {[}27$ \times $27$ \times $96{]}                                                                            &                                                                                            & 3$ \times $3                                                                                     & 2                                       &                                          & 0                                        \\ \hline
CONV2                                  & {[}27$ \times $27$ \times $256{]}                                                                           & 256                                                                                        & 5$ \times $5                                                                                     & 1                                       & 2                                        & 5$ \times $5$ \times $96$ \times $256                               \\ \hline
POOL2                                  & {[}13$ \times $13$ \times $256{]}                                                                           &                                                                                            & 3$ \times $3                                                                                     & 2                                       &                                          & 0                                        \\ \hline
CONV3                                  & {[}13$ \times $13$ \times $384{]}                                                                           & 384                                                                                        & 3$ \times $3                                                                                     & 1                                       & 1                                        & 3$ \times $3$ \times $256$ \times $384                              \\ \hline
CONV4                                  & {[}13$ \times $13$ \times $384{]}                                                                           & 384                                                                                        & 3$ \times $3                                                                                     & 1                                       & 1                                        & 3$ \times $3$ \times $383$ \times $384                              \\ \hline
CONV5                                  & {[}13$ \times $13$ \times $256{]}                                                                           & 256                                                                                        & 3$ \times $3                                                                                     & 1                                       & 1                                        & 3$ \times $3$ \times $383$ \times $256                              \\ \hline
POOL3                                  & {[}6$ \times $6$ \times $256{]}                                                                             &                                                                                            & 3$ \times $3                                                                                     & 2                                       &                                          & 0                                        \\ \hline
FC6                                    &     {[}1$ \times $1$ \times $4096{]}                                                                                      &                                                                                            &                                                                                         &                                         &                                          & 6$ \times $6$ \times $256$ \times $4096                             \\ \hline
FC7                                    &     {[}1$ \times $1$ \times $4096{]}                                                                                      &                                                                                            &                                                                                         &                                         &                                          & 4096$ \times $4096                                \\ \hline
FC8                                    &  {[}1$ \times $1$ \times $51{]}                                                                                         &                                                                                            &                                                                                         &                                         &                                          & 4096$ \times $51                                  \\ \hline
\end{tabular}
\caption{Caffenet in detail. Representation at each step of the processing and the total number of weights at each layer. Most of the parameters are used in last FC layers.}
\label{table:caffenet}
\end{table}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Input Layer}
Input layer has the raw pixel values of the image, in our case an image of width 227, height 227, and with three color channels R, G and B. Input Layer type we have used is LMDB.

\subsubsection{Convolutional Layer}
The convolutional layer  consists of kernels/filters which have a receptive field. During forward pass, the filter convolves around the image computing the dot product between the entries of the filter and the input producing 2-dimensional ``activations'' of the filter. These filters learn specific features which activate when similar feature elsewhere in the image. There can be many such filters in a convolutional layer. Stacking these ``activations'' for all filters along the depth dimension forms the full output of that convolution layer. Visualization of the Convolutional layer's ``activations'' and filters can be seen in Figures \ref{fig:viz1}, \ref{fig:viz2} and \ref{fig:viz3}

Covolutional layer can have hyper-parameters like Receptive field (F) or size of filter, the number of filters (K), Stride(S) with which we slide the filter. When the stride is 1 then we move the filters one pixel at a time, Size of zero-padding(P) padding the input volume with zeros around the border. It allows us to control the spatial size of the output of the filter.\\
If the filter receives an input volume of size W1$ \times $H1$ \times $D1, 
Produces a volume of size W2$ \times $H2$ \times $D2 where:\\
\begin{equation}
\begin{multlined}
W2=\frac{W1-F+2P}{S}+1 \\
H2=\frac{H1-F+2P}{S}+1 \\
D2=K\\
\end{multlined}
\end{equation}
With parameter sharing, it introduces F$ \times $F$ \times $D1 weights per filter, for a total of (F$ \times $F$ \times $D1)$ \times $K weights and K biases.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{ReLu Layer}
ReLU(Rectified Linear Unit) layer which is commonly used activation function will apply an element-wise activation function, such as the $ f(x)=max(0,x) $ which is thresholding at zero. During the backpropagation the derivative for ReLU is: \\
\begin{equation}
 \frac{\mathrm{d} y}{\mathrm{d} x}=\left\{\begin{matrix}
1 & x>  \varepsilon \\ 
0 & x\leqslant \varepsilon 
\end{matrix}\right.
\end{equation}
where $ \varepsilon  $ is a very small real number close to zero.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.4\textwidth]{Images/relu.jpeg}
    \caption{Rectified Linear Unit (ReLU) activation function, which is zero when x less than 0 and then linear with slope 1 when x greater than 0. Image from  \cite{cnnstanford}}
    \label{fig:relu}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\subsubsection{Pooling Layer}
The pooling layer will perform a non-linear down-sampling operation. One of the famous non-linear function used for pooling is max pooling. During max pooling the input is partitioned into non-overlapping rectangles and for each sub-region, it outputs the maximum value. But the depth dimension remains unchanged. This operation is shown in Figure \ref{fig:maxpooling}. Its function is to slowly reduce the size of the input to reduce the parameters of the network and to make it scale and translation invariant. A max pooling layer is often inserted in between successive convolution layers. Visualization of the max pooling layer can be seen in Figure \ref{fig:viz2} (left).
It has hyper-parameters: Filter size(F), Stride(S).\\
If the filter receives an input volume of size W1$ \times $H1$ \times $D1, 
Produces a volume of size W2$ \times $H2$ \times $D2 where:\\
\begin{equation}
\begin{multlined}
W2=\frac{W1-F}{S}+1\\
H2=\frac{H1-F}{S}+1\\
D2=D1 \\
\end{multlined}
\end{equation}

Introduces zero parameters since it computes a fixed function of the input.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{Images/maxpooling.png}
    \caption{ Max pooling with a 2$ \times $2 filter and stride = 2. Image taken from \cite{maxpooling}}
    \label{fig:maxpooling}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Local Response Normalization Layer}
The local response normalization layer(LRN) is used to normalize pixels across channels in the first convolutional layers and perform ``lateral inhibition''. It normalizes activations from neurons of different layers so that further layers are not biased to overly active or inactive neurons of the present layer. Visualization of LRN layer can be seen in Figure \ref{fig:viz2}(right)
 \cite{imagenetcnn}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Dropout Layer}
The dropout layer is a regularization method which reduces over-fitting preventing complex co-adaptations on the training data. In Caffenet dropout layers with dropout ratio 0.5 have been used which means each hidden unit is randomly removed from the network with a probability of 0.5 during the training phase. 


 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Fully Connected Layer}
Each neuron in this layer will be connected to all the neurons in the previous layer. It has no structure like in convolutional layers so parameter sharing is much lesser. They are usually used to compute class scores so they have a size of [1$ \times $1$ \times $number of classes]. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{SoftMaxWithLoss Layer ?}
%%%%%%%%%%%%%%%%% Redo
Loss is calculated to drive learning by comparing output value to a target value and assigning a cost to minimize. In general, a loss layer specifies how the network training penalizes the deviation between the predicted and true labels and is normally the last layer in the network \cite{caffelayers}. The softmax loss layer computes the multinomial logistic loss softmax of its input, also called cross entropy loss, is given by:
\begin{equation}
H(p,q)=-\sum p(x) \> \text{log} \> q(x) 
\end{equation}

$ p(x) $ is ``true'' distribution where all probability mass is on correct classification. And $ q(x) $ probability assigned to the correct label $ y_i $ given the image $ x_i $ and parameterized by SoftMax weights $ W $ 
\begin{equation}
 P(y_i | x_i;W)=\frac{e^{f_{y_i}}}{\sum_j e^{f_j}}
\end{equation}
where $ f $ is output vector of the last fully connected layer.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55555
\subsection{Forward and backward pass}

In forward pass, network computes the output given the input for the layer. This pass goes from bottom to top (Data layer to Loss layer). The data $ x $ is passed through an inner product layer for $ g(x) $ then through a softmax for $ h(g(x)) $ and softmax loss to give $ f_w(x)  $ = $H(p,q)$ multinomial logistic
loss of the softmax layer.

In backward pass the network calculates the gradient of each layer to bottom.
The backward pass begins with the loss layer and computes the gradient with respect to the output $ \frac{\partial f_w}{\partial h}  $. The gradient with respect to the rest of the model is computed layer-by-layer through the chain rule. Layers with parameters, compute the gradient with respect to their parameters $  \frac{\partial f_w}{\partial W_{layer}}  $ during the backward step.

To solve optimization problem \cite{caffesolver} of loss minimization we use Stochastic gradient descent which weights $ W $ by a linear combination of the negative gradient $ \Delta L(W) $   and the previous weight update $ V_t $. The learning rate $ \alpha $ is the weight of the negative gradient. The momentum $ \eta $ is the weight of the previous update.

To compute the update value $ V_{t+1} $ and the updated weights $ W_{t+1} $ at iteration $ t+1 $, given the previous weight update $ V_t $ and current weights $ W_t $:
\begin{equation}
\begin{multlined}
 V_{t+1}=\eta V_t - \alpha \Delta L(W_t) \\
 W_{t+1}=W_t+V_{t+1}\\
 \end{multlined}
\end{equation} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Visualization}

We used Deep-visualization-toolbox \cite{visualization} to generate neuron-by-neuron visualizations of Caffenet. Few snapshots are shown here.

%\begin{figure}[h!]
%    \centering
%    \includegraphics[width=0.3\textwidth]{Images/test_image.png}
%    \caption{ Test image used in visualizations.}
%    \label{fig:testimage}
%\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/viz1.png}
    \caption{Left: Visualization of first CONV layer filter. It has total 96 filters. Right: The activations produced by filters in CONV1 layer. We can see that interesting features like edges are detected.}
    \label{fig:viz1}
\end{figure}



% \begin{figure}[h!]
%   \centering
%   \begin{minipage}[b]{0.4\textwidth}
%     \includegraphics[width=\textwidth]{Images/conv1_filters.png}
%     \caption{Visualization of first CONV layer filter. It has total 96 filters.}
%     \label{fig:conv1_filter}
%   \end{minipage}
%   \hfill
%   \begin{minipage}[b]{0.4\textwidth}
%     \includegraphics[width=\textwidth]{Images/conv1_activation.png}
%     \caption{The activations produced by filters in CONV1 layer. We can see that interesting features like edges are detected.}
%     \label{fig:conv1_activation}
%   \end{minipage}
% \end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/viz2.png}
    \caption{Left: Visualization of first POOL layer. We can observe that the images are down-sampled. Right: Visualization of first LRN layer. We can observe that neurons with large activations are boosted.}
    \label{fig:viz2}
\end{figure}

% \begin{figure}[h!]
%   \centering
%   \begin{minipage}[b]{0.4\textwidth}
%     \includegraphics[width=\textwidth]{Images/pool1_activation.png}
%     \caption{Visualization of first POOL layer. We can observe that the images are down-sampled.}
%     \label{fig:pool1_activation}
%   \end{minipage}
%   \hfill
%   \begin{minipage}[b]{0.4\textwidth}
%     \includegraphics[width=\textwidth]{Images/lrn.png}
%     \caption{Visualization of first LRN layer. We can observe that neurons with large activations are boosted.}
%     \label{fig:lrn1}
%   \end{minipage}
% \end{figure}


\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/viz3.png}
    \caption{Left: Visualization of second CONV layer filter. It has total 256 filters. Right: The activations produced by filters in CONV2 layer. We can see much more interesting features detected.}
    \label{fig:viz3}
\end{figure}



% \begin{figure}[h!]
%   \centering
%   \begin{minipage}[b]{0.4\textwidth}
%     \includegraphics[width=\textwidth]{Images/conv2_filters.png}
%     \caption{Visualization of second CONV layer filter. It has total 256 filters.}
%     \label{fig:conv2_filter}
%   \end{minipage}
%   \hfill
%   \begin{minipage}[b]{0.4\textwidth}
%     \includegraphics[width=\textwidth]{Images/conv2_activation.png}
%     \caption{The activations produced by filters in CONV2 layer. We can see many more interesting features detected.}
%     \label{fig:conv2_activation}
%   \end{minipage}
% \end{figure}




