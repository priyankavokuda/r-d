\chapter{Implementation details: Object Proposals }
\lhead{\emph{Implementation details: Object Proposals }} 



\section{Overview}
Many object detection algorithms are based on sliding window approach applied at every location of the image. To increase the efficiency of these object detection methods, objects proposals are first generated, which serve as input to object detection methods. In this work, we use Edge Boxes \cite{zitnick2014edge} method to generate object bounding box proposals. In Edge Boxes method, an object is said to be enclosed in a bounding box if there are contours completely contained in that bounding box. A score called ``objectness'' score is assigned to each bounding box which is measured by the number of edges that exist in the box minus those that are members of contours that overlap the box’s
boundary. This score is generated for bounding boxes generated at each location, scale and aspect ratio.
The contours are derived from Edge detection techniques using Structured Random Forests \cite{dollar2013structured}, explained in coming section. The depth data is added to RGB data during edge detection part.

\section{Edge Boxes}

For a given image, we find the probability of a location to contain edge also called ``edge responses'' by using Structured Edge Detector. Structured Edge Detector gives us a dense edge response map. To obtain sparse edge response, it is further subjected to Non-Maximal Suppression (NMS) where the non-maximal edge responses orthogonal to maximal edge responses are suppressed. The resulting sparse edge map which has edge magnitude $ m_p $ and orientation $ \theta_p $ for each pixel $ p $. Edges with high edge responses are contours, which are edges forming a coherent boundary. For computational ease, we combine the edges to form edge groups. Eight connected edges are combined until the sum of their orientation difference is above a predetermined threshold to form edge groups $ S $. Edge groups are shown in figure \ref{fig:edgeboxes}, row three. Affinity $ a(s_i,s_j) $ between edge groups $s_i$ and $s_j$ with positions $ x_i $ $ x_j $, is given by:
\begin{equation}
  a(s_i,s_j) = \left | cos(\theta_i-\theta_{ij} cos(\theta_j-\theta_{ij})) \right |^\gamma 
\end{equation}
$ \theta_{ij} $ is the angle between $ x_i $ and $ x_j $. The value of $ \gamma $ = 2 is generally used. Next step is finding the ``objectness'' score of the bounding box $ b $. For each edge group we calculate a ? continuous valued variable $ w_b(s_i)  \in [0,1] $ which determines if it is completely in the bounding box or not. $ S_b $  are set of edge groups which lie on bounding box $ b $. For all the edge groups $ s_i $ which lie on $ S_b $  are set to $ w_b(s_i) = 0 $. To calculate $ w_b(s_i) $ edge groups $ s_i  \notin S_b $ , we assume a path of edge groups $ T $
with path length $ \left | T \right | $. We assume this path begins at edge group present on the boundary of the bounding box and ends at an edge group say $ s_i $. To find if the path exists, we find the affinity between the adjacent edge groups in the path. If there is no such path we assign $ w_b(s_i) = 1 $ else $ w_b(s_i) = 0 $. It is computed by:
\begin{equation}
  w_b(s_i)=1- max\prod_{j}^{\left | T \right |-1}a(t_j-t_{j+1}) 
\end{equation}

Using all the computed $ w_b $ we can next find ``objectness'' score. We first compute the sum of magnitudes of all edges $ p $ in edge group $ s_i $ say $ m_i $.
The score of the bounding box $ b $ of height $ b_h $ and width $ b_w $ is given by:
\begin{equation}
  h_b=\frac{\sum_i w_b(s_i)m_i }{2(b_w+b_h)^\kappa } 
\end{equation}
where $ \kappa $ = 1.5 is used  ??

\begin{figure}[h!]
    \centering
    \includegraphics[width=1.0\textwidth]{Images/edgeboxes.png}
    \caption{Figure showing (first row) original image,
(second row) Structured edges detected from Structured edge detector, (third row) edge groups, (fourth row) example
correct bounding box and edge labeling, and (fifth row) Green edges are predicted to be part of the object in the box ($ w_b(s_i) = 1 $ ),
while red edges are not ($ w_b(s_i) = 0 $ ). Image borrowed from \cite{zitnick2014edge}}
    \label{fig:edgeboxes}
\end{figure}

\subsection{Parameters}

The accuracy of a bounding box is calculated using Intersection over Union (IoU) metric also called $ \delta $. It has the intersection of the computed bounding box and ground truth bounding box divided by the area of the union of these bounding boxes. A lower value of $ \delta $ can result in finding higher objects but with lower accuracy and higher value of $ \delta $ can lead to finding lower number of objects but with higher accuracy. IoU is graphically illustrated in \ref{fig:iou}

\begin{figure}[h!]
    \centering
    \includegraphics[width=1.0\textwidth]{Images/iou.png}
    \caption{Figure showing a graphical illustration of Intersection of Union (IoU) and random bounding boxes with IoU
of 0.25, 0.5,0.75 and 0.97.}
    \label{fig:iou}
\end{figure}



The bounding boxes are computed using sliding window approach over a different location, scale and aspect ratio. The step size of such of these parameters are adjusted to generate fixed value $ \alpha $. The scale value ranges from $ \delta = 1000 $ pixels to the whole image. The aspect ratio varies from $ \frac{1}{\tau} $. $ \tau $ = 3 is used in practice.


\section{Edge detection}
\label{sec:edge}
Edges have a property that it can capture high level and informative information from the image and they are sparser compared to the original image. Structured Random forests are used in Edge Boxes for the process of edge detection. 

\subsection{Random Forests}

Random forest is an ensemble of decision trees. Decision trees are the hierarchical tree-like representations consisting nodes and branches. During decision tree learning, it looks into attributes in data to split the data into subsets. Information gain is measured at each split to achieve best results. We use Gini impurity \cite{dollar2013structured} as information gain measure. Gini impurity is the probability of a randomly sampled variable being classified incorrectly if it was randomly labeled according to the distribution of labels in the subset. It is computed by adding the probability of picking data with a particular label multiplied by the probability of wrongly splitting the item. The splitting is continued until the subsets are pure and the results are stored in the leaf node. One disadvantage of decision trees is it can easily overfit the data. Random trees, on the other hand, is a collection of decision trees whose input is a random subset of the actual data. The final result of the random forest is the mean or mode of the result of individual decision trees. In our application, we use random forests to perform edge detection.  

\subsection{Structured Random Forests}

Structured Random forests are based on the concept of structured learning \cite{nowozin2011structured}. These learning methods are used in learning particular structure rather than learning discrete or real values. Edges have inherent spatial structure like lines, T-junctions or Y-junctions. A random forests model \cite{breiman2001random} is used here which learns structures rather than discrete or real values, hence the name structured random forests. The random forest learns local segmentation masks given input image patches. Structured labels are used to decide the splitting function of the tree whose information gain is measured by mapping structured labels to discrete space.

Let $ X $ be an image and $ Y $ be the annotation output space of that image. $ x \in X $
denotes image patch and $ y \in Y $ denotes local image annotation (segmentation masks). During the training of the forest, splits are performed mapping structured labels $ y \in Y $ at a node to a discrete set of labels $ c \in C $, where $ C = {1,.....,k} $ where similarly structured labels are associated with same discrete label $ c $. To calculate information gain, a mapping from $ Y $ space to an intermediate space $ Z $ in which distance is easily measured is defined  which is later mapped to space $ C $. The mapping is defined as $ z=\Pi(y) $ where $ z $ is a long binary vector that encodes whether every pair of pixels in y belong to same or different segments. To calculate information gain, the structured labels $ y \in Y $ is mapped $c \in C$ such that labels with similar $ z $ are assigned to same discrete label $ c $. This makes to easy to use information gain criteria like Gini impurity. The leaf nodes of the tree store learned segmentation masks. The prediction of multiple trees is combined through averaging.

Depth data is combined with RGB image to form a four channeled image which is used in this structured random forest model. The random forest is able to use the information of curvature present in depth data as curvature is obtained from a linear combination of depth data. Two different trees could look into implicit perpendicular vectors of the data encoding the surface normal information.   


\subsubsection{Parameters}
 Each tree was trained using one million randomly sampled patches. There were eight trees in the random forest. The result of applying structured edge detector is shown in figure \ref{fig:edges}
 
 \begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\textwidth]{Images/edgeDetector.png}
    \caption{Figure showing of edge detection results on the NYU Depth Dataset V2 \cite{Silberman:ECCV12}
dataset: (top row) original image, (second row) depth image, (third
row) ground truth segmentation values, (fourth
row) edges detected from the structured edge detector}
    \label{fig:edges}
\end{figure}


%\section{Experiments}


